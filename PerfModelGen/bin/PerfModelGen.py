"""Performance Model Generator Main file"""
import sys
import os
from os.path import expanduser
import re
import logging
import cProfile
import pstats
import datetime
import configargparse
import sys
sys.path.append("D:\\PDT_integration\\RTool+K-Mean\\PerfModelGen")

import bin.PerfModelCreator
from bin.RInputCreator import inputcreator
from bin.RCreateGen import RCreateGen



__all__ = []
__version__ = 0.1
__date__ = "2016-05-09"
__updated__ = "2017-02-17"
__toolname__ = "PerfModelGen"

PROFILE = 0
LOGGER = logging.getLogger(__toolname__)
FILE_INPUT_LIST = {}
FILE_CREATE_LIST = {}

def initialize_config_parser(program_path):
    """Intializing configuration"""
    try:
        system_config_path_temp = "../config/PerModelGen.conf"
        system_config_path = os.path.abspath(program_path+"/"+system_config_path_temp)
        home_config_temp = expanduser("~/.PerfModelGen/PerfModelGen.conf")
        home_config_path = os.path.abspath(home_config_temp)

        #print absConfigPath
        args = configargparse.ArgParser(default_config_files=[system_config_path, home_config_path])

        args.add('-c', '--my-config', required=False, is_config_file=True, help='config file path')
        args.add('-i', '--input_folder_path', required=True, \
            help='Input folder path of input files')
        args.add('-o', '--output_folder_path', required=True, \
            help='Output folder path of output files shall be saved')
        args.add('-l', '--log_folder_path', required=True, \
            help='Log folder path where log files would be created')
        args.add('-g', '--sorted_graph_limit', required=False, \
            help='Limit for number of sorted graphs')
        args.add('-d', '--diplay_all_combination', required=False, \
            help='Display all binary combination')
        args.add('-mp', '--mapping_input_column', required=True, help='Mapping input column')
        args.add('-mn', '--mapping_input_names', required=True, help='Mapping input names')
        args.add('-dc', '--data_column', required=False, help='Data column')
        args.add('-sc', '--start_row', required=True, help='Start row')
        args.add('-ec', '--end_row', required=True, help='End row')
        args.add('-id', '--identifiers', required=False, help='Separator identifiers')
        args.add('-if', '--input_file_name', required=True, help='Input file')
        args.add('-ic', '--index_column', required=True, help='Index Column')
        args.add('-ms', '--measurement_sheet', required=True, help='Measurement Sheet')
        args.add('-hd', '--header_row_num', required=True, help='Header Row No')
        args.add('-a', '--approach', required=True, help='Approach')
        args.add('-mo', '--mode', required=True, help='Mode')
        args.add('-j', '--judge_criteria', required=True, help='Judge Criteria')
        args.add('-mt', '--method', required=True, help='Define Method to check regular/irregular behavior')
        args.add('-indata','--input_col_as_data', required=True, help='Check regular/irrregular')
        args.add('-clean_col','--clean_input_col', required=True, help='whether to run regular/irregular on input column')
        args.add('-f','--factor', required=True, help='defined factor')
    except Exception as e_temp:
        print (e_temp)
    return args

def initialize_logger(logpath=""):
    """initialize logger"""
    # create logger
    LOGGER.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)

    # create file handler and set level to debug
    file_handler = logging.FileHandler(logpath+__toolname__+"_"+\
            datetime.datetime.now().strftime("%Y%m%d-%H%M%S")+".log")
    file_handler.setLevel(logging.DEBUG)

    # create formatter
    formatter_ch = logging.Formatter("%(message)s")
    formatter_fh = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

    # add formatter to ch
    console_handler.setFormatter(formatter_ch)
    file_handler.setFormatter(formatter_fh)

    # add ch to logger
    LOGGER.addHandler(console_handler)
    LOGGER.addHandler(file_handler)
    return 0

def input_file_enumerator(inputparams):
    """input file enumerator function"""
    infolder_path = inputparams.input_folder_path
    outfolder_path = inputparams.output_folder_path
    file_list = os.listdir(outfolder_path)

    for infile in file_list:
        minput = re.match("R_input_(.*).csv", infile)
        if minput:
            create_file = "R_script_create_"+minput.group(1)+".csv"
            if os.path.exists(outfolder_path+create_file):
                FILE_INPUT_LIST[minput.group(1)] = infile
                FILE_CREATE_LIST[minput.group(1)] = create_file
            else:
                LOGGER.warn("Corresponding create file not found for : "+infile)
    if len(FILE_INPUT_LIST) == 0:
        LOGGER.error("R Input file(s) pair not found in "+os.path.abspath(infolder_path))
        return -1

    LOGGER.info("FileListKeys: "+str(FILE_INPUT_LIST.keys()))
    return 0

def create_perf_models(inputparams):
    """create performance model function"""
    for key in FILE_INPUT_LIST.keys():
        LOGGER.info("Starting Model Creation for : "+str(key))
        bin.PerfModelCreator.create_perf_model(key, \
            inputparams.output_folder_path+FILE_INPUT_LIST[key], \
            inputparams.output_folder_path+FILE_CREATE_LIST[key], inputparams.output_folder_path, \
            int(inputparams.sorted_graph_limit), LOGGER)
        LOGGER.info("Completed Model Creation for : "+str(key))
    return 0


def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_path = os.path.dirname(os.path.abspath(sys.argv[0]))
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = "Program Name:%s %s (%s)" % \
    (program_name, program_version, program_build_date)


    args = initialize_config_parser(program_path)

    inputparams = args.parse_args()

    if not os.path.isdir(inputparams.log_folder_path):
        print ("Log path does not exist at: "+inputparams.log_folder_path)
        sys.exit(2)

    inputparams.log_folder_path = inputparams.log_folder_path + "/"
    initialize_logger(inputparams.log_folder_path)
    LOGGER.info("### PerfModelGen Execution Start ###")
    LOGGER.info("Program Information:"+program_version_message)
    LOGGER.debug("Input Values Considered:" + str(args.parse_args()))
    LOGGER.debug("Input Values Source:" + str(args.format_values()))

    if not os.path.isdir(inputparams.input_folder_path):
        logging.error("Input path does not exist at: "+inputparams.input_folder_path)
        sys.exit(2)
    if not os.path.isdir(inputparams.output_folder_path):
        logging.error("Output path does not exist at:  "+inputparams.output_folder_path)
        sys.exit(2)
    if not (inputparams.sorted_graph_limit.isdigit() and \
    (inputparams.sorted_graph_limit == str(abs(int(inputparams.sorted_graph_limit))))):
        inputparams.sorted_graph_limit = 0
        LOGGER.warn("Sorted graph limit is not appropriate hence it will be ignored")

    if not inputparams.factor.isdigit():
        logging.error("factor number should be integer value")
        sys.exit(2)

    if inputparams.start_row.isdigit() and inputparams.end_row.isdigit():
        if (int(inputparams.end_row) - int(inputparams.start_row)) < 0:
            logging.error("Start row number("+inputparams.start_row+") should not be greater than end row number("+inputparams.end_row+")")
            sys.exit(2)
    else:
        logging.error("Start and end row number should be integer value")
        sys.exit(2)
    inputparams.input_folder_path = inputparams.input_folder_path + "/"
    inputparams.output_folder_path = inputparams.output_folder_path + "/"

    inputcreator(inputparams, LOGGER)
    r_create_gen = RCreateGen()

    r_create_gen.create_r_script(inputparams.output_folder_path, \
        inputparams.output_folder_path, inputparams.diplay_all_combination, LOGGER)
    input_file_enumerator(inputparams)

    create_perf_models(inputparams)

    LOGGER.debug("### PerfModelGen Execution End ###")
    return 0

if __name__ == '__main__':

    if PROFILE:
        PROFILE_FILENAME = "../logs/PerfModelgen_profile_"+ \
            datetime.datetime.now().strftime("%Y%m%d-%H%M%S")+".txt"
        cProfile.run('main()', PROFILE_FILENAME)
        STATS_FILE = open("../logs/PerfModelgen_stats_"+ \
            datetime.datetime.now().strftime("%Y%m%d-%H%M%S")+".txt", "wb")
        PROFILE_STATS = pstats.Stats(PROFILE_FILENAME, stream=STATS_FILE)
        STATS = PROFILE_STATS.strip_dirs().sort_stats("cumulative")
        STATS.print_stats()
        STATS_FILE.close()
        sys.exit(0)
    sys.exit(main())
