"""R outlier CSV and R input CSV Generator"""
import os
import sys
import math
import itertools
import openpyxl
import xlrd
from pyper import R

from xlsxwriter.workbook import Workbook
from bin.lib.xlprocessor import processinput, validateinput
from bin.algos.kmean.classifier import classify
from bin.lib.judge import *
from bin.lib.iohandle import generatesheets
#from compiler.pycodegen import EXCEPT

R_SCRIPT = '''
a<-c(INPUT)
b<-boxplot(a)
c<-b$out
d<-length(c)
'''

RTOOL = R(RCMD="C:\\Program Files\\R\\R-3.4.1\\bin\\R")


def outlier(inputlist, logger):
    """finding outliers for individual scenario"""
    try:
        val = str(inputlist).replace("[", "").replace("]", "")

        # check values in inputlist
        checkrowno(val, logger)
        cmd = R_SCRIPT.replace("INPUT", val)

        # Run r tool command for boxplot
        RTOOL.run(cmd)
        elements = RTOOL.get("c")
        count = RTOOL.get("d")
        out = list()
        ret = list()
        if count == 1:
            out.append(elements)
        else:
            out = elements

        new_inputlist = list()
        new_out = list()
        # normalize values upto 4 decimal place
        for i_temp in inputlist:
            new_inputlist.append("%.4f" % round(i_temp, 4))
        for j_temp in out:
            new_out.append("%.4f" % round(j_temp, 4))
        # If outlier exist in inputlist append true in list
        for data in new_inputlist:
            if data in new_out:
                ret.append(True)
            else:
                ret.append(False)
    except Exception:
        logger.debug("Problem occur in outlier function")
        sys.exit(2)
    return ret


def modeprocessor(inputs, outlierlist, scenariolist, logger):
    """Process mode1 and mode2"""
    logger.debug("Mode " + inputs.mode + " starts processing")
    try:
        index = scenariolist[0].split(inputs.identifiers)[0]
        outliers = 0
        count = 0
        prev_count = 0
        ret = list()
        if int(inputs.judge_criteria) < 0 or int(inputs.judge_criteria) > 100:
            logger.debug("Wrong Judge Criteria Selected in setting file. \
            It should be greater than equal to 0 and less than equal to 100")
            sys.exit(2)
        # Mode 1: Check if %regular > judge criteria, if not exit
        if int(inputs.mode) == 1:
            try:
                if inputs.method is "R-Box":
                    regular_perc = (outlierlist.count(False) / float(len(outlierlist))) * 100
                else:
                    regular_perc = (outlierlist.count(1) / float(len(outlierlist))) * 100

            except ZeroDivisionError as e_temp:
                print (e_temp)
                logger.error("Modeprocessor function: Divide by zero error. Outlier list is empty")
                sys.exit(2)
            if int(regular_perc) >= int(inputs.judge_criteria):
                ret = outlierlist
            else:
                logger.debug(
                    "Judge option calculation of Mode " + inputs.mode + " failed.")
                sys.exit(2)
        # Mode 2: Check for each scenario if %regular > judge criteria, if not discard scenario
        elif int(inputs.mode) == 2:
            for scenario in scenariolist:
                i_temp = scenariolist.index(scenario)
                if index != scenariolist[i_temp].split(inputs.identifiers)[0] or \
                        i_temp == (len(scenariolist) - 1):
                    index = scenariolist[i_temp].split(inputs.identifiers)[0]
                    if i_temp == (len(scenariolist) - 1):
                        count += 1
                        if outlierlist[i_temp] is False or outlierlist[i_temp] == 1:
                            outliers += 1
                    if int((outliers / float(count)) * 100) >= int(inputs.judge_criteria):
                        for j_temp in range(prev_count, (prev_count + count)):
                            ret.append(outlierlist[j_temp])
                    else:
                        for j_temp in range(count):
                            ret.append(True)
                    prev_count += count
                    if outlierlist[i_temp] is False or outlierlist[i_temp] == 1:
                        outliers = 1
                    else:
                        outliers = 0
                    count = 1
                else:
                    if outlierlist[i_temp] is False or outlierlist[i_temp] == 1:
                        outliers += 1
                    count += 1
        else:
            logger.debug(
                "Wrong Mode Selected in setting file. Mode value can be 1 or 2")
            sys.exit(2)
    except Exception:
        logger.debug("Mode " + inputs.mode + " not executed successfully")
        sys.exit(2)
    return ret


def groups(inputs, sheet, datacol, logger):
    """group each scenario for finding outliers"""
    logger.debug("It is groups function")
    logger.debug("Creating R_outlier_" + str(sheet[datacol + \
        str(int(inputs.start_row) - 1)].value).split("_")[0].replace('-', '') + ".csv file")
    try:
        index = str(sheet[inputs.index_column + \
                inputs.start_row].value).split(inputs.identifiers)[0]
        elements = list()
        outlierlist = list()
        scenariolist = list()
        for i in range(int(inputs.start_row), int(inputs.end_row) + 1):
            if (index != str(sheet[inputs.index_column + \
                str(i)].value).split(inputs.identifiers)[0]) or (i == int(inputs.end_row)):
                if i == int(inputs.end_row):
                    elements.append(sheet[datacol + str(i)].value)
                outlierlist = outlierlist + outlier(elements, logger)
                elements = list()
                index = str(sheet[inputs.index_column +
                                  str(i)].value).split(inputs.identifiers)[0]
            if index == str(sheet[inputs.index_column +
                                  str(i)].value).split(inputs.identifiers)[0]:
                scenariolist.append(
                    str(sheet[inputs.index_column + str(i)].value))
            elements.append(sheet[datacol + str(i)].value)
        inputfile = open(inputs.output_folder_path + "R_outlier_" + str(sheet[datacol + \
            str(int(inputs.header_row_num))].value).split("_")[0].replace('-', '') + ".csv", "w")
        for scenario in scenariolist:
            i = scenariolist.index(scenario)
            row_value = str(scenariolist[i]) + "," + str(outlierlist[i])
            inputfile.write(str(row_value) + ",\n")
        inputfile.close()
        logger.debug("R_outlier_" + str(sheet[datacol + \
            str(int(inputs.header_row_num))].value).split("_")[0].replace('-', '') + \
            ".csv file created Successfully")
    except Exception:
        logger.debug("R_outlier_" + str(sheet[datacol + \
            str(int(inputs.header_row_num))].value).split("_")[0].replace('-', '') + \
            ".csv file not created")
        sys.exit(2)
    return outlierlist, scenariolist


def approachprocessor(inputs, outliers, sheet, datacol, logger):
    """Process approach selected by user"""
    try:
        inputfile = open(inputs.output_folder_path + "R_input_" + str(sheet[datacol + \
            str(int(inputs.header_row_num))].value).split("_")[0].replace('-', '') + ".csv", "w")
        header = inputs.mapping_input_names
        input_cols = inputs.mapping_input_column.split(",")
        rows = int(inputs.end_row) - int(inputs.start_row) + 1
        if inputs.method is "R-Box":
            num_outliers = outliers.count(True)
        else:
            num_outliers = outliers.count(1)
        # Approach 1 - Alternative Approach
        if int(inputs.approach) == 1:
            new_header = header.split(',')
            for line in new_header:
                header = header + "," + str(line) + "_Sqr"

            # To add + as input parameter combination uncomment these comment
            #for line in range(2, len(new_header) + 1):
            #    for subset in itertools.combinations(new_header, line):
            #        temp_header = ""
            #        for i in subset:
            #            if temp_header != "":
            #               temp_header = temp_header + "_P_" + str(i)
            #            else:
            #                temp_header = str(i)
            #        header = header + "," + temp_header
            for line in new_header:
                header = header + "," + str(line) + "_mod" + str(rows - num_outliers)
            for line in new_header:
                header = header + "," + "log_" + str(line)
            header = header + "," + str(sheet[datacol + \
                str(int(inputs.header_row_num))].value.split("_")[0].replace('-', ''))
            inputfile.write(str(header) + "\n")
            for i in range(int(inputs.start_row), int(inputs.end_row) + 1):
                val = outliers.pop(0)
                #if not val:
                if val is False or val == 1:
                    data = ""
                    for inputcol in input_cols:
                        if len(data) > 0:
                            data = data + ","
                        data = data + str(sheet[inputcol + str(i)].value)
                    cpu_value = str(sheet[datacol + str(i)].value)
                    new_data = data.split(',')
                    for line in new_data:
                        data = data + "," + str(pow(int(line), 2))

                    # To add + as input parameter combination uncomment these comment
                    #for line in range(2, len(new_data) + 1):
                    #    for subset in itertools.combinations(new_data, line):
                    #        temp_data = 0
                    #        for i in subset:
                    #            temp_data = temp_data + int(i)
                    #        data = data + "," + str(temp_data)
                    for line in new_data:
                        data = data + "," + str(int(line) % (rows - num_outliers))
                    for line in new_data:
                        data = data + "," + str(math.log(int(line), 2))
                    data = data + "," + cpu_value
                    inputfile.write(str(data) + "\n")
        # Approach 0 - Normal Approach
        elif int(inputs.approach) == 0:
            header = header + "," + str(sheet[datacol + \
                str(int(inputs.header_row_num))].value.split("_")[0].replace('-', ''))
            inputfile.write(str(header) + "\n")
            for i in range(int(inputs.start_row), int(inputs.end_row) + 1):
                val = outliers.pop(0)
                #if not val:
                if val is False or val == 1:
                    data = ""
                    for inputcol in input_cols:
                        if len(data) > 0:
                            data = data + ","
                        data = data + str(sheet[inputcol + str(i)].value)
                    data = data + "," + str(sheet[datacol + str(i)].value)
                    inputfile.write(str(data) + "\n")
        else:
            logger.debug(
                "Wrong Approach Selected in setting file. Approach value can be 0 or 1")
            inputfile.close()
            sys.exit(2)
        inputfile.close()
    except Exception:
        logger.debug("Approach not processed successfully")
        sys.exit(2)


def inputcreator(inputs, log):
    """R outlier and R input creator"""
    logger = log
    input_list = ""
    scenerio_list=""
    # try:
    filepath = inputs.input_folder_path + inputs.input_file_name
    input_cols = inputs.mapping_input_column.split(",")
    input_col_names = inputs.mapping_input_names.split(",")
    secenerio_col = inputs.index_column

    if not os.path.isfile(filepath):
        logger.error("Error " + filepath + " Not found")
        sys.exit(2)

    work_book = openpyxl.load_workbook(filepath)
    logger.debug("##### Start Process for R_Input file ####")
    logger.debug("Excel Sheet Name-->" + inputs.measurement_sheet)

    try:
        sheet = work_book.get_sheet_by_name(inputs.measurement_sheet)
    except Exception:
        logger.debug(
            inputs.measurement_sheet + " name does not exist in excel file")
        sys.exit(2)

    if (inputs.method == "R-Box"):
        for inputcol in input_cols:
            for i in range(int(inputs.start_row), int(inputs.end_row) + 1):
                if not input_list:
                    input_list = str(sheet[inputcol + str(i)].value)
                else:
                    input_list = input_list + "," + \
                        str(sheet[inputcol + str(i)].value)
            # Call checkrowno function
            checkrowno(input_list, logger)

        for datacol in inputs.data_column.split(","):
            logger.debug("R_input_" + str(sheet[datacol + \
                str(int(inputs.header_row_num))].value).split("_")[0].replace('-', '') + ".csv")

            # Call group function
            outlierlist, scenariolist = groups(inputs, sheet, datacol, logger)

            # Call mode processor function
            outliers = modeprocessor(inputs, outlierlist, scenariolist, logger)

            if len(input_col_names) != len(input_cols):
                logger.error("Error in input - mapping_input_column \
                    is not mapped to mapping_input_names")
                sys.exit(2)
            approachprocessor(inputs, outliers, sheet, datacol, logger)
    elif (inputs.method == "K-Mean"):
        for i in range(int(inputs.start_row), int(inputs.end_row) + 1):
            if not scenerio_list:
                scenerio_list = str(sheet[secenerio_col + str(i)].value)
            else:
                scenerio_list = scenerio_list + "," + \
                             str(sheet[secenerio_col + str(i)].value)

        #scenerio_list = scenerio_list.tolist()
        scenerio_list = [x.strip() for x in scenerio_list.split(',')]

        feature_data_frame, label_data_frame, input_as_dataframe, index_data_frame = processinput(inputs, log)
        class_data_frame = classify(label_data_frame.copy(),index_data_frame.copy(), inputs)
        itr=len(class_data_frame.columns)

        col= [x.strip() for x in inputs.data_column.split(',')]
        for i in range (len(class_data_frame.columns)):
            outlierlist=class_data_frame[class_data_frame.columns[i]].tolist()

            # Call mode processor function
            outliers = modeprocessor(inputs, outlierlist, scenerio_list, logger)

            if len(input_col_names) != len(input_cols):
                logger.error("Error in input - mapping_input_column \
                                is not mapped to mapping_input_names")
                sys.exit(2)
            approachprocessor(inputs, outliers, sheet, col[i], logger)


            inputfile_outlier = open(
                inputs.output_folder_path + "R_outlier_" + str(class_data_frame.columns[i]).split("_")[1] + ".csv",
                "w")
            for scenario in scenerio_list:
                i = scenerio_list.index(scenario)
                row_value = str(scenerio_list[i]) + "," + str(outlierlist[i])
                inputfile_outlier.write(str(row_value) + ",\n")
            inputfile_outlier.close()
    else:
        logger.debug("Invalid method argument passed in configuration file")
        sys.exit(2)
    # except Exception:
    #     logger.debug("R_Input file is not created Successfully")
    #     sys.exit(2)
    return 0


def checkrowno(value, logger):
    """ check row number"""
    try:
        arrayintergerlist = value.split(",")
        count = 0
        for i in range(arrayintergerlist.__len__()):
            # if value in arrayintergerlist is float
            if arrayintergerlist[i].find('L') < 0:
                float(arrayintergerlist[i])
                count += 1
            # if value in arrayintergerlist is integer
            else:
                int(arrayintergerlist[i])
                count += 1
    except (TypeError, ValueError):
        logger.debug(
            "Please Enter correct input/output column or Row no. in which real number data exist")
        sys.exit(2)
