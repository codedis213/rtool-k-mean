"""R create CSV generator"""
import os
import re
from bin.CSVWriter import CSVUtility



#logger = None

# this class is user for creating R_create_script file . this file use as a input in R_tool
# it take R_input_A.csv file as a input and reproduce csv file
ATTRIBUTELIST = []

class RCreateGen():
    """R script create generator class"""

    def __init__(self):
        """default constructor"""
        self.name = ""

    # create R_create_script.csv file
    def create_r_script(self, inputpath, outputpath, display, logger):
        """"create R script function"""
        try:
            filelistdata = input_enumerator(inputpath)
            logger.debug("#START PROCESSING FOR R_SCRIPT_CREATER#")
            # getting input file R_input file path from configuration file
            for filename in filelistdata:
                logger.info("#########Start#############")
                if not os.path.exists(inputpath + filename):
                    logger.warn("input CSV file path is not exist")
                    return False

                # read number of column of R_input file
                for line in open(inputpath + filename):
                    filecolumns = line.split(',')
                    attributename = filecolumns[filecolumns.__len__() - 1]
                    attributename = attributename.replace("\n", "")
                    break
                csvrows = []
                logger.info(filename + " file have " + str(filecolumns.__len__()) + " no. of column")
                no_com = noofcombination(filecolumns.__len__())
                binarystring = binarycombination(no_com, filecolumns.__len__(), display)
                logger.info(filename + " file have " + str(no_com) + " binary combination")
    
                headers = []
                headers.append("target_value")
                headers.append("R Variable name")
                for k in range(filecolumns.__len__() - 1):
                    headers.append("x$" + filecolumns[k])

                headers.append("intersection")
                headers.append("glmname")
                headers.append("1")
                headers.append("2")
                headers.append("3")
                headers.append("4")
                headers.append("5")

                for cmb in range(binarystring.__len__()):
                    binarycomb = binarystring[cmb].split(",")
                    data = []
                    data.append(attributename + str(cmb + 1))
                    data.append("x$" + attributename.split("_")[0].replace('-', ''))
                    glmname = "glm_" + attributename.split("_")[0].replace('-', '')
                    glmcreater = ""

                    for var in range(filecolumns.__len__()):
                        data.append(binarycomb[var])
                        if var < filecolumns.__len__() - 1:
                            if binarycomb[var] == '1':
                                glmname += chr(ord('C') + var)
                                glmcreater += headers[var + 2] + "+"
    
                    formulapostfix = ""
                    if binarycomb[filecolumns.__len__() - 1] == '1':
                        glmname += "_0"
                        formulapostfix = "+0"
    
                    formula = glmname
                    formula += "<-glm("
                    formula += "x$" + attributename.split("_")[0].replace('-', '') + "~"
                    formula += glmcreater
    
                    data.append(glmname)
                    formula = formula[:-1]
                    data.append(formula + formulapostfix + ")")
                    data.append("min(" + glmname + "$residuals)")
                    data.append("max(" + glmname + "$residuals)")
                    data.append("median(" + glmname + "$residuals)")
                    data.append("coef(summary(" + glmname + "))")
                    csvrows.append(data)
                logger.info("Creating Output CSV file")
                writer = CSVUtility()
                writer.csv_writer(outputpath + "R_script_create_" + \
                     attributename.replace('-', '') + ".csv", headers, csvrows, logger)
                logger.info("R_script_create_" + attributename + ".CSV file Creating Successfully")
                logger.info("#########End#############")
        except Exception:
            logger.error("R_script_create_" + attributename + \
                '.CSV file not Creating Successfully')


#this function is used to calculate no of combination 2 power no. of input column
def noofcombination(noofcolumns):
    """calculate no. of binary combination """
    if noofcolumns == 0:
        return 1
    count = 2
    for i in range(noofcolumns - 1):
        count = count * 2
    return count


#this function take two input  noofcombination(column) and noofrowsflag. it gives binary combination
def binarycombination(combination, columns, flag):
    """binary combination function"""
    binarystring = []
    reversebinary = []
    for var in range(combination):
        binary = ""
        for rev in reversed(range(columns)):
            binary += str(var // noofcombination(rev) % 2) + ','
        binary = binary[:-1]
        binarystring.append(binary)

    if flag == "no":
        binarystring.pop(0)
        binarystring.pop(0)

#reverse binary String in sequence
    for var in reversed(binarystring):
        reversebinary.append(var)

    return reversebinary


def input_enumerator(folderpath):
    """input file enumerator"""
    file_input_list = []
    file_list = os.listdir(folderpath)

    for infile in file_list:
        minput = re.match("R_input_(.*).csv", infile)
        if minput:
            create_file = "R_input_" + minput.group(1) + ".csv"
            if os.path.exists(folderpath + create_file):
                file_input_list.append(infile)
                ATTRIBUTELIST.append(minput.group(1))
            else:
                print ("Corresponding create file not found for : " + infile)
    if file_input_list.__len__() == 0:
        print ("R Input file(s) pair not found in " + os.path.abspath(folderpath))
        # logger.error("R Input file(s) pair not found in "+os.path.abspath(folderPath))

    return file_input_list
