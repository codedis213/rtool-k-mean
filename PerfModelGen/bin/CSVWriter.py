"""CSV Writer file"""
import csv
import sys
import codecs


class CSVUtility:
    """CSV utility class"""
    def __init__(self):
        """default constructor"""
        self.mode = "wb"

# this function use to download CSV file of given path with given header
# and data
    def csv_writer(self, path, header, data, logger):
        """CSV Write function"""
        try:
            logger.debug("#CSV writer Class#")
            with codecs.open(path, 'wb', encoding='ISO-8859-1') as csv_file:
                spamwriter = csv.writer(csv_file, delimiter=',', \
                    quotechar=',', quoting=csv.QUOTE_MINIMAL)
                spamwriter.writerow(header)
                logger.debug("#Write Data into CSV file#")
                for i in range(data.__len__()):
                    spamwriter.writerow(data[i])
                logger.debug("OUTPUT CSV file is created")
        except Exception:
            logger.warn("Please Close output file.")
            sys.exit(0)
