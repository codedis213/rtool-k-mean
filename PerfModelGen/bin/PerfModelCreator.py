"""Performance Model creator"""
import os
import sys
import string
import csv
import numpy
from pyper import R
import openpyxl
from xlsxwriter.workbook import Workbook
import codecs

R_SCRIPT1 = '''
x<- read.csv("INPUT_FILE",,encoding="ISO-8859-1",header=T,sep=",")
input_file_check<-any(x=="")
input_header<-names(x)
'''

R_SCRIPT2 = '''
inputCreateFile <- read.csv("INPUT_CREATE_FILE",header=T,sep=",",stringsAsFactors=F,encoding="ISO-8859-1")
create_file_check<-any(inputCreateFile=="")
create_header<-names(inputCreateFile)
result_header<-c(create_header,"t value","Minimum Deviance Residual","Max Deviance Residual","Median Deviance Residual","Coefficient","t value detail")
'''

R_SCRIPT3 = '''
inputCreateFile <- read.csv("INPUT_CREATE_FILE",header=T,sep=",",stringsAsFactors=F,encoding="ISO-8859-1")
create_file_check<-any(inputCreateFile=="")
eval(parse(text=inputCreateFile[,"X1"]))
Y1<-inputCreateFile[,1]
Y2<-sapply(inputCreateFile[,"X2"], function(x) eval(parse(text=x)))
Y3<-sapply(inputCreateFile[,"X3"], function(x) eval(parse(text=x)))
Y4<-sapply(inputCreateFile[,"X4"], function(x) eval(parse(text=x)))
Y5<-sapply(inputCreateFile[,"X5"], function(x) list(eval(parse(text=x))))
Y6<-sapply(Y5, function(x) toString(paste(rownames(x),x[,"t value"])))
Y7<-sapply(Y5, function(x) toString(paste(rownames(x),x[,"Estimate"])))
Y8<-sapply(Y5, function(x) x[,"Estimate"])
Y15<-as.character(sapply(Y5, function(x) all(x[,"t value"] >5)))
Y15[is.na(Y15)]<-"NA"
Y15[Y15=="TRUE"]<-"t>5"
Y15[Y15=="FALSE"]<-"t<=5"
datafunc<-function(a)
{
Z<-sapply(Y5, function(x) toString(try(x[a,"Estimate"],silent=TRUE)))
Z<-gsub(".*subscript.*","0",Z)
Z
}
colName<-c(paste("x$",head(names(x),-1),sep=""),"(Intercept)")
BB<-sapply(colName,datafunc)
BB<-matrix(BB,,length(input_header))
DD<-apply(BB,1,paste,collapse=",")
Y14<-paste(Y1,DD,sep=",")
result<-cbind(inputCreateFile,Y15,Y2,Y3,Y4,Y7,Y6)
'''

CLEAR = '''
rm(list=setdiff(ls(), "x"))
input_header<-names(x)
'''

GLOSSARY = '''
S.No.,Topics,Description
1,Diff% value lies between +- 30%,Total count of difference (measured vs target) in +- 30% range.
2,Sum of absolute value of difference,Sum of absolute value of difference % (measured vs target)
3,Average of difference,Average of difference % (measured vs target)
4,Ratio,It is ratio of (Total count of difference (measured vs target) in +- 30% range) and Total count
'''

SUMMARY_HEADER = '''
Performance Model                                    
Model Analysis                                
target_value(*)
'''

PERF_MODEL_HEADER = '''
Model Analysis                                
target_value(*)
'''

R_PERFMODELS = None
R_RESULT = None
R_RESULT_HEADER = None
R_DATA_HEADER = None
R_DATA = None
R_COFF = None
DIFF_PERC_TOP = None
SUM_OF_ABS_TOP = None
AVG_DIFF_TOP = None
RATIO_TOP = None
LOGGER = None
SUMMARY_SHEET = None
WORKSHEET_DIFF = None
WORKSHEET_SUM = None
WORKSHEET_AVG = None
WORKSHEET_RATIO = None

def execute_r(createfile, inputfile, outputfolder):
    """Execution R Script"""
    global R_PERFMODELS
    R_PERFMODELS = list()
    global R_RESULT
    R_RESULT = list()
    global R_RESULT_HEADER
    R_RESULT_HEADER = list()
    global R_DATA
    R_DATA = list()
    global R_COFF
    R_COFF = list()
    global R_DATA_HEADER
    R_DATA_HEADER = list()
    temp_file = str(os.path.abspath(outputfolder)+"\\temporary_file.csv")
    temp_file = temp_file.replace("\\","/")
    
    script = R_SCRIPT1.replace('INPUT_FILE', inputfile)
    check_file_script = R_SCRIPT2.replace('INPUT_CREATE_FILE', createfile)
    LOGGER.debug("Initializing R")

    # generate a R instance
    #rtool = R()
    rtool = R(RCMD="C:\\Program Files\\R\\R-3.4.1\\bin\\R")

    LOGGER.debug("Executing R script")

    ret = rtool.run(script)
    ret1 = rtool.run(check_file_script)

    if "Error" in ret or "Error" in ret1:
        LOGGER.debug(ret)
        LOGGER.critical("Error in executing R")
        return -1

    input_check_file = rtool.get("input_file_check")

    if input_check_file != False:
        LOGGER.error("Input file is not Valid: "+inputfile)
        return -1

    # print (rtool.get("x").values.tolist())
    result_header = rtool.get("result_header")
    R_DATA_HEADER = rtool.get("input_header")
    R_DATA.extend(rtool.get("x").values.tolist())

    #Count total no. of lines in R Script Create CSV
    f = codecs.open(createfile, "r", encoding='ISO-8859-1')
    reader = csv.reader(f, delimiter=",")

    for lines in reader:
        line1 = lines
        break

    for lines in reader:

        resultFyle = codecs.open(temp_file,"wb",encoding='ISO-8859-1')
        wr = csv.writer(resultFyle, dialect='excel')
        wr.writerow(line1)
        wr.writerow(lines)
        del wr
        resultFyle.close()
        script1 = R_SCRIPT3.replace('INPUT_CREATE_FILE', temp_file)

        ret = rtool.run(script1)
        
        if "Error" in ret:
            LOGGER.debug(ret)
            LOGGER.critical("Error in executing R")
            return -1
        create_check_file = rtool.get("create_file_check")
        if create_check_file != False:
            LOGGER.error("R_input file is not Valid: "+createfile)
            return -1

        R_PERFMODELS.append(rtool.get("Y1"))
        #print (rtool.get("result"))
        #print (rtool.get("result").tolist())
        R_RESULT.extend(rtool.get("result").values.tolist())
        R_COFF.append(rtool.get("Y14"))
        rtool.run(CLEAR)

    os.remove(temp_file)
    LOGGER.debug("R execution completed ")
    for i_temp in result_header:
        R_RESULT_HEADER.append(i_temp.replace("x.", "x$").replace(".", " "))

    if not isinstance(R_COFF, list):
        y_tmp = R_COFF
        R_COFF = list()
        R_COFF.append(y_tmp)
    return 0

def add_index(workbook, inputlist):
    """Add the standard url link format."""

    url_format = workbook.add_format({'font_color': 'blue', 'underline':  1})
    index = 1
    worksheet = workbook.add_worksheet(name="Index")
    worksheet.write(2, 2, "S.No")
    worksheet.write(2, 3, "Sheet Name")
    worksheet.write(3, 2, "1")
    worksheet.write_url(3, 3, 'internal:Glossary!A1', url_format, "Glossary")
    worksheet.write(4, 2, "2")
    worksheet.write_url(4, 3, 'internal:Result!A1', url_format, "Result")
    worksheet.write(5, 2, "3")
    worksheet.write_url(5, 3, 'internal:Summary!A1', url_format, "Summary")
    worksheet.write(6, 2, "4")
    worksheet.write_url(6, 3, 'internal:GraphSortedByCountDiff!A1', \
                         url_format, "GraphSortedByCountDiff")
    worksheet.write(7, 2, "5")
    worksheet.write_url(7, 3, 'internal:GraphSortedBySum!A1', url_format, "GraphSortedBySum")
    worksheet.write(8, 2, "6")
    worksheet.write_url(8, 3, 'internal:GraphSortedByAverage!A1', \
                        url_format, "GraphSortedByAverage")
    worksheet.write(9, 2, "7")
    worksheet.write_url(9, 3, 'internal:GraphSortedByRatio!A1', url_format, "GraphSortedByRatio")

    return 0

def add_glossary(workbook):
    """Add glossary in performance model sheet"""

    worksheet = workbook.add_worksheet(name="Glossary")
    reader = csv.reader(GLOSSARY.split('\n'), delimiter=',')
    for r_temp, row in enumerate(reader):
        for c_temp, col in enumerate(row):
            worksheet.write(r_temp+3, c_temp+2, col)
    return 0

def add_result(workbook):
    """Add result sheet"""

    worksheet = workbook.add_worksheet(name="Result")
    for c_temp, col in enumerate(R_RESULT_HEADER):
        worksheet.write(0, c_temp, col)
    for r_temp, row in enumerate(R_RESULT):
        for c_temp, col in enumerate(row):
            if isinstance(col, str) or isinstance(col, int) or isinstance(col, numpy.floating):
                worksheet.write(r_temp+1, c_temp, col)
            else:
                worksheet.write(r_temp + 1, c_temp, col.decode('utf-8'))
    return 0

def add_summary(key, workbook, glimit, outputfolder):
    """Add summary sheet"""

    global DIFF_PERC_TOP
    global SUM_OF_ABS_TOP
    global AVG_DIFF_TOP
    global RATIO_TOP
    directory = os.path.abspath(outputfolder)+"\\PerfModelSheets_"+key+"\\"

    worksheet = SUMMARY_SHEET
    reader = csv.reader(SUMMARY_HEADER.split('\n'), delimiter=',')
    for r_temp, row in enumerate(reader):
        for c_temp, col in enumerate(row):
            worksheet.write(r_temp-1, c_temp, col)
    for r_temp, row in enumerate(R_COFF):
        elements = row.split(',')
        for c_temp, col in enumerate(elements):
            worksheet.write(r_temp+3, c_temp, col)
            if r_temp == 0 and c_temp > 0:
                worksheet.write(2, c_temp, string.ascii_lowercase[c_temp-1])

    diff_perc = list()
    sum_of_abs = list()
    avg_diff = list()
    ratio = list()

    for r_temp, row in enumerate(R_PERFMODELS):
        excel_sheet = "PerformanceModel_"+row+".xlsx"
        wb = openpyxl.load_workbook(directory+excel_sheet)
        sheet = wb.get_sheet_by_name(excel_sheet.replace(".xlsx", ""))

        #diff_perc.append(sheet[string.ascii_uppercase[len(R_DATA_HEADER)+3]+"7"].value)
        diff_perc.append(sheet[string.ascii_uppercase[len(R_DATA_HEADER)+4]+"7"].value)
        sum_of_abs.append(sheet[string.ascii_uppercase[len(R_DATA_HEADER)+5]+"7"].value)
        avg_diff.append(sheet[string.ascii_uppercase[len(R_DATA_HEADER)+6]+"7"].value)
        ratio.append(sheet[string.ascii_uppercase[len(R_DATA_HEADER)+7]+"7"].value)
        worksheet.write(r_temp+3, len(R_DATA_HEADER)+1, \
                        sheet[string.ascii_uppercase[len(R_DATA_HEADER)+3]+"7"].value)
        worksheet.write(r_temp+3, len(R_DATA_HEADER)+2, \
                        sheet[string.ascii_uppercase[len(R_DATA_HEADER)+4]+"7"].value)
        worksheet.write(r_temp+3, len(R_DATA_HEADER)+3, \
                        sheet[string.ascii_uppercase[len(R_DATA_HEADER)+5]+"7"].value)
        worksheet.write(r_temp+3, len(R_DATA_HEADER)+4, \
                        sheet[string.ascii_uppercase[len(R_DATA_HEADER)+6]+"7"].value)
        worksheet.write(r_temp+3, len(R_DATA_HEADER)+5, \
                        sheet[string.ascii_uppercase[len(R_DATA_HEADER)+7]+"7"].value)

        if r_temp == 0:
            worksheet.write(r_temp+2, len(R_DATA_HEADER)+1, \
                            sheet[string.ascii_uppercase[len(R_DATA_HEADER)+3]+"6"].value)
            worksheet.write(r_temp+2, len(R_DATA_HEADER)+2, \
                            sheet[string.ascii_uppercase[len(R_DATA_HEADER)+4]+"6"].value)
            worksheet.write(r_temp+2, len(R_DATA_HEADER)+3, \
                            sheet[string.ascii_uppercase[len(R_DATA_HEADER)+5]+"6"].value)
            worksheet.write(r_temp+2, len(R_DATA_HEADER)+4, \
                            sheet[string.ascii_uppercase[len(R_DATA_HEADER)+6]+"6"].value)
            worksheet.write(r_temp+2, len(R_DATA_HEADER)+5, \
                            sheet[string.ascii_uppercase[len(R_DATA_HEADER)+7]+"6"].value)
            worksheet.write(1, 1, sheet['F1'].value)

    # get top N(=glimit) values of diff%, sum of absolute, average of difference% and ratio
    DIFF_PERC_TOP = sorted(range(len(diff_perc)), key=lambda i: diff_perc[i], reverse=True)[:glimit]
    SUM_OF_ABS_TOP = sorted(range(len(sum_of_abs)), key=lambda i: sum_of_abs[i], \
                            reverse=True)[:glimit]
    AVG_DIFF_TOP = sorted(range(len(avg_diff)), key=lambda i: avg_diff[i], reverse=True)[:glimit]
    RATIO_TOP = sorted(range(len(ratio)), key=lambda i: ratio[i], reverse=True)[:glimit]

    return 0

def col_to_excel(col):
    """ col is 1 based"""

    excel_col = str()
    div = col
    while div:
        (div, mod) = divmod(div-1, 26) # will return (x, 0 .. 25)
        excel_col = chr(mod + 65) + excel_col

    return excel_col

def add_sorted_chart(workbook, worksheet, index, datarow, sheetname, x_temp=0, y_temp=0):
    """Create a new chart object. In this case an embedded chart."""

    chart1 = workbook.add_chart({'type': 'scatter'})
    endrow = datarow + 2
    categories = "1:" + str(endrow)

    dataindex = col_to_excel(27+index*3)
    values = "="+sheetname+"!$" + dataindex + "2:$" + dataindex + str(endrow)

    # Configure the first series.
    chart1.add_series({'name':"="+sheetname+"!$"+dataindex+'2', 'categories': categories, \
                       'values':values,})

    dataindex = col_to_excel(28+index*3)
    values = "="+sheetname+"!$" + dataindex + "2:$" + dataindex + str(endrow)
    # Configure the first series.
    chart1.add_series({
        'name':       "="+sheetname+"!$" + dataindex + '2',
        'categories': categories,
        'values':     values,
    })

    # Configure second series. Note use of alternative syntax to define ranges.
    dataindex = col_to_excel(29+index*3)
    values = "="+sheetname+"!$" + dataindex + "2:$" + dataindex + str(endrow)

    chart1.add_series({'name':"="+sheetname+"!$"+dataindex+'2', 'categories':categories, \
    'values':values, 'trendline': {'type': 'linear', 'name': 'Trend-Target', \
    'line': {'color': 'green', 'width': 3,},},})

    # Add a chart title and some axis labels.
    chart1.set_title({'name': 'Performance Graph'})

    # Set an Excel chart style. Colors with white outline and shadow.
    chart1.set_style(10)

    # Insert the chart into the worksheet (with an offset).
    worksheet.insert_chart(x_temp, y_temp, chart1, {'x_offset': 25, \
                'y_offset': 10, 'width': 720, 'height': 576})

    return 0

def add_sorted_data(key, sheet_name, worksheet, r_temp, columindex, glimit, outputfolder):
    """add sorted data function"""

    directory = os.path.abspath(outputfolder)+"\\PerfModelSheets_"+key+"\\"
    formula_1 = "=INDEX(Summary!A4:A"+str(len(R_RESULT)+3)+",MATCH(LARGE(Summary!"+ \
    columindex+"4:"+columindex+str(len(R_RESULT)+3)+",RANK),Summary!"+ \
    columindex+"4:"+columindex+str(len(R_RESULT)+3)+",0))"

    worksheet.write_formula(r_temp*20+3, 0, formula_1.replace('RANK', str(r_temp+1)))
    formula_2 = "=VLOOKUP(A"+str(r_temp*20+4)+",Result!A2:"+ \
    string.ascii_uppercase[len(R_DATA_HEADER)+2]+\
    str(len(R_RESULT)+2)+","+str(len(R_DATA_HEADER)+3)+",FALSE)"

    worksheet.write_formula(r_temp*20+3, 1, formula_2)
    worksheet.write_formula(r_temp*20+4, 2, "=Summary!"+ \
                            string.ascii_uppercase[len(R_DATA_HEADER)+2]+"3")
    worksheet.write_formula(r_temp*20+4, 3, "=Summary!"+ \
                            string.ascii_uppercase[len(R_DATA_HEADER)+3]+"3")
    worksheet.write_formula(r_temp*20+4, 4, "=Summary!"+ \
                            string.ascii_uppercase[len(R_DATA_HEADER)+4]+"3")
    worksheet.write_formula(r_temp*20+4, 5, "=Summary!"+ \
                            string.ascii_uppercase[len(R_DATA_HEADER)+5]+"3")

    formula_3 = "=VLOOKUP(A"+str(r_temp*20+4)+",Summary!A4:"+ \
    string.ascii_uppercase[len(R_DATA_HEADER)+5]+str(len(R_RESULT)+3)+ \
    ","+str(len(R_DATA_HEADER)+3)+",FALSE)"
    worksheet.write_formula(r_temp*20+5, 2, formula_3)

    formula_4 = "=VLOOKUP(A"+str(r_temp*20+4)+",Summary!A4:"+ \
    string.ascii_uppercase[len(R_DATA_HEADER)+5]+str(len(R_RESULT)+3)+ \
    ","+str(len(R_DATA_HEADER)+4)+",FALSE)"
    worksheet.write_formula(r_temp*20+5, 3, formula_4)

    formula_5 = "=VLOOKUP(A"+str(r_temp*20+4)+",Summary!A4:"+ \
    string.ascii_uppercase[len(R_DATA_HEADER)+5]+str(len(R_RESULT)+3)+ \
    ","+str(len(R_DATA_HEADER)+5)+",FALSE)"
    worksheet.write_formula(r_temp*20+5, 4, formula_5)

    formula_6 = "=VLOOKUP(A"+str(r_temp*20+4)+",Summary!A4:"+ \
    string.ascii_uppercase[len(R_DATA_HEADER)+5]+str(len(R_RESULT)+3)+ \
    ","+str(len(R_DATA_HEADER)+6)+",FALSE)"
    worksheet.write_formula(r_temp*20+5, 5, formula_6)

    try:
        i_temp = sheet_name
        for c_temp in range(len(R_DATA)+1):
            worksheet.write_formula(c_temp+1, 26+r_temp*3, \
            "=\'"+directory+"[PerformanceModel_"+i_temp+".xlsx]PerformanceModel_"+ \
            i_temp+"\'!"+string.ascii_uppercase[len(R_DATA_HEADER)-1]+str(c_temp+5))

            worksheet.write_formula(c_temp+1, 27+r_temp*3, \
            "=\'"+directory+"[PerformanceModel_"+i_temp+".xlsx]PerformanceModel_"+ \
            i_temp+"\'!"+string.ascii_uppercase[len(R_DATA_HEADER)]+str(c_temp+5))

            worksheet.write_formula(c_temp+1, 28+r_temp*3, \
            "=\'"+directory+"[PerformanceModel_"+i_temp+".xlsx]PerformanceModel_"+ \
            i_temp+"\'!"+string.ascii_uppercase[len(R_DATA_HEADER)+1]+str(c_temp+5))
    except Exception:
        LOGGER.debug("PerfModelCreator: Problem in add_sorted_data function.")
        sys.exit(2)

    return 0

def add_diff_graph(key, workbook, glimit, outputfolder):
    """Add difference graph"""
    sheet_name_list = list()
    for i_temp in DIFF_PERC_TOP:
        sheet_name_list.append(R_PERFMODELS[i_temp])
    worksheet = WORKSHEET_DIFF
    #workbook.add_worksheet(name="GraphSortedByCountDiff")
    worksheet.write(0, 0, "Sort by")
    worksheet.write(0, 1, "Count Difference")

    columindex = string.ascii_uppercase[len(R_DATA_HEADER)+2]

    for r_temp in range(min(glimit, len(R_RESULT))):
        add_sorted_data(key, str(sheet_name_list.pop(0)), worksheet, \
                        r_temp, columindex, glimit, outputfolder)
        add_sorted_chart(workbook, worksheet, r_temp, len(R_DATA), \
                          "GraphSortedByCountDiff", r_temp*20+7, 1)

    return 0

def add_sum_graph(key, workbook, glimit, outputfolder):
    """Add sum graph"""
    sheet_name_list = list()
    for i_temp in SUM_OF_ABS_TOP:
        sheet_name_list.append(R_PERFMODELS[i_temp])
    worksheet = WORKSHEET_SUM
    #workbook.add_worksheet(name="GraphSortedBySum")
    worksheet.write(0, 0, "Sort by")
    worksheet.write(0, 1, "Sum")

    columindex = string.ascii_uppercase[len(R_DATA_HEADER)+3]

    for r_temp in range(min(glimit, len(R_RESULT))):
        add_sorted_data(key, str(sheet_name_list.pop(0)), \
                         worksheet, r_temp, columindex, glimit, outputfolder)
        add_sorted_chart(workbook, worksheet, r_temp, len(R_DATA), \
                         "GraphSortedBySum", r_temp*20+7, 1)

    return 0

def add_avg_graph(key, workbook, glimit, outputfolder):
    """Add avergae graph"""
    sheet_name_list = list()
    for i_temp in AVG_DIFF_TOP:
        sheet_name_list.append(R_PERFMODELS[i_temp])
    worksheet = WORKSHEET_AVG
    #workbook.add_worksheet(name="GraphSortedByAverage")
    worksheet.write(0, 0, "Sort by")
    worksheet.write(0, 1, "Average")

    columindex = string.ascii_uppercase[len(R_DATA_HEADER)+4]

    for r_temp in range(min(glimit, len(R_RESULT))):
        add_sorted_data(key, str(sheet_name_list.pop(0)), \
                         worksheet, r_temp, columindex, glimit, outputfolder)
        add_sorted_chart(workbook, worksheet, r_temp, len(R_DATA), \
                         "GraphSortedByAverage", r_temp*20+7, 1)

    return 0

def add_ratio_graph(key, workbook, glimit, outputfolder):
    """Add ratio graph"""
    sheet_name_list = list()
    for i_temp in RATIO_TOP:
        sheet_name_list.append(R_PERFMODELS[i_temp])
    worksheet = WORKSHEET_RATIO
    #workbook.add_worksheet(name="GraphSortedByRatio")
    workbook.add_worksheet
    worksheet.write(0, 0, "Sort by")
    worksheet.write(0, 1, "Ratio")

    columindex = string.ascii_uppercase[len(R_DATA_HEADER)+5]

    for r_temp in range(min(glimit, len(R_RESULT))):
        add_sorted_data(key, str(sheet_name_list.pop(0)), \
                        worksheet, r_temp, columindex, glimit, outputfolder)
        add_sorted_chart(workbook, worksheet, r_temp, len(R_DATA), \
                         "GraphSortedByRatio", r_temp*20+7, 1)

    return 0

def add_sorted_graphs(key, workbook, glimit, outputfolder):
    """Add sorted graphs"""

    if glimit == 0:
        glimit = len(R_RESULT)
    add_diff_graph(key, workbook, glimit, outputfolder)
    add_sum_graph(key, workbook, glimit, outputfolder)
    add_avg_graph(key, workbook, glimit, outputfolder)
    add_ratio_graph(key, workbook, glimit, outputfolder)
    return 0

def add_sorted_worksheet(workbook):
    """Add sorted worksheet"""

    global SUMMARY_SHEET
    global WORKSHEET_DIFF
    global WORKSHEET_SUM
    global WORKSHEET_AVG
    global WORKSHEET_RATIO
    SUMMARY_SHEET = workbook.add_worksheet(name="Summary")
    WORKSHEET_DIFF = workbook.add_worksheet(name="GraphSortedByCountDiff")
    WORKSHEET_SUM = workbook.add_worksheet(name="GraphSortedBySum")
    WORKSHEET_AVG = workbook.add_worksheet(name="GraphSortedByAverage")
    WORKSHEET_RATIO = workbook.add_worksheet(name="GraphSortedByRatio")
    return 0

def add_chart(workbook, worksheet, data_row, sheet_name, header_length, x_temp=0, y_temp=0):
    """Create a new chart object. In this case an embedded chart."""

    chart1 = workbook.add_chart({'type': 'scatter'})
    endrow = data_row + 5
    categories = "1:" + str(endrow)

    values = "PerformanceModel_" + sheet_name + "!$" + \
    string.ascii_uppercase[header_length-1] + "6:$" + \
    string.ascii_uppercase[header_length-1] + str(endrow)

    workbook.define_name(sheet_name+'_Measured', '='+values)
    # Configure the first series.
    chart1.add_series({
        'name':       '=PerformanceModel_' + sheet_name + '!$' + \
        string.ascii_uppercase[header_length-1] + '$5',
        'categories': categories,
        'values':     values,
    })

    values = "PerformanceModel_" + sheet_name + "!$" + \
    string.ascii_uppercase[header_length] + "6:$" + \
    string.ascii_uppercase[header_length] + str(endrow)

    workbook.define_name(sheet_name+'_Target', '='+values)
    # Configure the first series.
    chart1.add_series({
        'name':       '=PerformanceModel_' + sheet_name + \
        '!$' + string.ascii_uppercase[header_length] + '$5',
        'categories': categories,
        'values':     values,
    })

    # Configure second series. Note use of alternative syntax to define ranges.
    values = "PerformanceModel_" + sheet_name + "!$" + \
    string.ascii_uppercase[header_length + 1] + "6:$" + \
    string.ascii_uppercase[header_length + 1] + str(endrow)

    workbook.define_name(sheet_name+'_Diff', '='+values)
    chart1.add_series({'name':'=PerformanceModel_'+sheet_name+'!$'+ \
    string.ascii_uppercase[header_length+1]+'$5', 'categories': categories, 'values':values, \
    'trendline':{'type':'linear', 'name': 'Trend-Target', 'line':{'color':'green', 'width':3,},},})

    # Add a chart title and some axis labels.
    chart1.set_title({'name': 'Performance Graph'})

# Set an Excel chart style. Colors with white outline and shadow.
    chart1.set_style(10)

# Insert the chart into the worksheet (with an offset).
    if x_temp == 0 and y_temp == 0:
        worksheet.insert_chart(8, header_length + 4, chart1, \
                               {'x_offset': 25, 'y_offset': 10, 'width': 720, 'height': 576})
    else:
        worksheet.insert_chart(x_temp, y_temp, chart1, \
                               {'x_offset': 25, 'y_offset': 10, 'width': 720, 'height': 576})

    return 0

def add_performance_model(workbook, sheetname, perfmodel, perfmodel_formula):
    """Add performance model function"""

    worksheet = workbook.add_worksheet(name="PerformanceModel_"+sheetname)
    reader = csv.reader(PERF_MODEL_HEADER.split('\n'), delimiter=',')
    for r_temp, row in enumerate(reader):
        for c_temp, col in enumerate(row):
            worksheet.write(r_temp-1, c_temp+4, col)

    worksheet.write(0, 5, "performance model(="+perfmodel+")")

    c_temp = 0
    for c_temp, col in enumerate(R_DATA_HEADER):
        worksheet.write(4, c_temp, col)
    worksheet.write(4, c_temp, R_DATA_HEADER[c_temp]+"(Measured)")
    worksheet.write(4, c_temp+1, R_DATA_HEADER[c_temp]+"(Target)")
    worksheet.write(4, c_temp+2, "Diff(%)")

    c_temp = 0
    measured_value = list()
    diff_value = list()
    total_count = 0
    diff_count = 0
    for r_temp, row in enumerate(R_DATA):
        measured_value.append(list(row)[-1])
        for c_temp, col in enumerate(row):
            worksheet.write(r_temp+5, c_temp, col)

    #print R_DATA
    for rc_temp, row1 in enumerate(R_COFF):
        coff = row1.split(',')
        if coff[0] == sheetname:
            del coff[0]
            for rd_temp, row in enumerate(R_DATA):
                data = list(row)
                del data[-1]
                data.append(1)
                value = sum([float(x) * float(y) for x, y in zip(data, coff)])
                try:
                    m_value = measured_value.pop(0)
                    diff = ((float(m_value) - value)*100)/m_value
                    diff_value.append(diff)
                    total_count = total_count + 1
                except Exception as ArithmeticError:
                    diff = 'Error'
                worksheet.write(rd_temp+5, c_temp+1, value)
                worksheet.write(rd_temp+5, c_temp+2, diff)

    sumofaverage = 0
    for diff  in diff_value:
        if diff < 30 and diff > -30:
            diff_count = diff_count + 1
        sumofaverage = sumofaverage + abs(diff)

    try:
        worksheet.write(5, len(R_DATA_HEADER)+3, "Total count Diff%")
        worksheet.write(6, len(R_DATA_HEADER)+3, total_count)
        worksheet.write(5, len(R_DATA_HEADER)+4, "Diff% value lies between +- 30%")
        worksheet.write(6, len(R_DATA_HEADER)+4, diff_count)
        worksheet.write(5, len(R_DATA_HEADER)+5, "Sum of absolute value of difference")
        worksheet.write(6, len(R_DATA_HEADER)+5, sumofaverage)
        worksheet.write(5, len(R_DATA_HEADER)+6, "Average of difference")
        worksheet.write(6, len(R_DATA_HEADER)+6, float(sumofaverage)/total_count)
        worksheet.write(5, len(R_DATA_HEADER)+7, "Ratio")
        worksheet.write(6, len(R_DATA_HEADER)+7, float(diff_count)/total_count)
    except Exception as ArithmeticError:
        print ("Arithmatic Exception")

    for r_temp, row in enumerate(R_COFF):
        elements = row.split(',')
        if elements[0] == sheetname:
            for c_temp, col in enumerate(elements):
                worksheet.write(2, c_temp+4, col)
                if c_temp > 0:
                    worksheet.write(1, c_temp+4, string.ascii_lowercase[c_temp-1])

    data_row = len(R_DATA)
    add_chart(workbook, worksheet, data_row, sheetname, len(R_DATA_HEADER))

    return 0

def add_performance_models(key, workbook, outputfolder):
    """Add performance models function"""

    perf_model = ""
    perf_model_formula = "="
    for c_temp, col in enumerate(R_DATA_HEADER):
        if c_temp < (len(R_DATA_HEADER)-1):
            perf_model = perf_model+string.ascii_lowercase[c_temp]+"*"+col+"+"
            perf_model_formula = perf_model_formula+string.ascii_uppercase[c_temp]+ \
            "ROWNUM*"+string.ascii_uppercase[c_temp+5]+"3+"
        else:
            perf_model = perf_model+string.ascii_lowercase[c_temp]
            perf_model_formula = perf_model_formula+string.ascii_uppercase[c_temp+5]+"3"

    directory = os.path.abspath(outputfolder)+"\\PerfModelSheets_"+key+"\\"

    if not os.path.exists(directory):
        os.makedirs(directory)
    for r_temp, row in enumerate(R_PERFMODELS):
        outputfile = directory+'PerformanceModel_'+row+'.xlsx'
        wb = Workbook(outputfile, {'strings_to_numbers': True})
        add_performance_model(wb, row, perf_model, perf_model_formula)
    return 0

def create_model_workbook(key, outputfolder, outputfile, glimit):
    """Create performance model workbook"""

    workbook = Workbook(outputfile, {'strings_to_numbers': True})
    add_index(workbook, R_PERFMODELS)
    LOGGER.debug("Index Sheet Created ")
    add_glossary(workbook)
    LOGGER.debug("Glossary Sheet Created ")
    add_result(workbook)
    LOGGER.debug("Result Sheet Created ")
    add_sorted_worksheet(workbook)
    add_performance_models(key, workbook, outputfolder)
    LOGGER.debug("All Performance Sheet Created ")
    add_summary(key, workbook, glimit, outputfolder)
    LOGGER.debug("Summary Sheet Created ")
    add_sorted_graphs(key, workbook, glimit, outputfolder)
    LOGGER.debug("Sorted Graph Sheet Created ")
    workbook.close()

    return 0

def create_perf_model(key, inputfile, createfile, outputfolder, glimit, log):
    """Create performance model function"""

    global LOGGER
    LOGGER = log
    outputfile = outputfolder+'PerformanceModel_'+key+'.xlsx'
    if execute_r(createfile, inputfile,outputfolder) != 0:
        return -1
    if create_model_workbook(key, outputfolder, outputfile, glimit) != 0:
        return -1

    LOGGER.info("outputFileName : "+os.path.abspath(outputfile))
    return 0
