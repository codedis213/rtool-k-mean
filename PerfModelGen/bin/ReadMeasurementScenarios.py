"""Read Measurement scenarios from Xlsx file"""
import os
import sys
import openpyxl
from bin.CSVWriter import CSVUtility


def reding_measurement_excel(input_file_path, output_file_path, columns_name, row, log):
    """Reading Measurement Excel"""
    global logger
    logger = log
    csv_data = []
    if not os.path.isfile(input_file_path):
        logger.error("Input file is not exist")
        sys.exit(2)
    wb_sheet = openpyxl.load_workbook(input_file_path)
    sheet = wb_sheet.get_sheet_by_name("performance measurement_reverse")
    for i in range(int(row), int(sheet.get_highest_row())+1):
        data = []
        for k in range(columns_name.__len__()):
            col = columns_name[k].split("=")
            if col.__len__() == 2:
                key = ""
                value = ""
                if col[0] != "":
                    key = col[0]+"="
                if "$" in  col[1]:
                    value = str(sheet[col[1]+str(i)].value)
                else:
                    value = str(col[1])
                data.append(key+value)
        print (data)
        csv_data.append(data)
    writer = CSVUtility()
    writer.csv_writer(output_file_path, [], csv_data, logger)
