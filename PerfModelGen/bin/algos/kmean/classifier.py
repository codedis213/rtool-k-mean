"""Implement regular irregular classifier using k-mean clustering"""
import sys

import numpy as np
import pandas as pd
import tensorflow as tf

from bin.lib.housekeeping import generate_scenario_array


def kmeanscluster(vector_values, num_clusters, max_num_steps, stop_coeficient=0.0):
    """K-mean clustering implemenation using tenosrflow"""
    vectors = tf.constant(vector_values)
    centroids = tf.Variable(tf.slice(tf.random_shuffle(vectors),
                                     [0, 0], [num_clusters, -1]))
    old_centroids = tf.Variable(tf.zeros([num_clusters, 2]))
    centroid_distance = tf.Variable(tf.zeros([num_clusters, 2]))

    expanded_vectors = tf.expand_dims(vectors, 0)
    expanded_centroids = tf.expand_dims(centroids, 1)

    distances = tf.reduce_sum(
        tf.square(tf.subtract(expanded_vectors, expanded_centroids)), 2)
    assignments = tf.argmin(distances, 0)

    """for c in range(num_clusters):
        t = tf.reduce_mean(tf.gather(vectors,tf.reshape(tf.where(tf.equal(assignments, c)), [1, -1])), reduction_indices=[1])"""


    means = tf.concat([
        tf.reduce_mean(
            tf.gather(vectors,
                      tf.reshape(
                          tf.where(
                              tf.equal(assignments, c)
                          ), [1, -1])
                      ), reduction_indices=[1])
        for c in range(num_clusters)],0)

    save_old_centroids = tf.assign(old_centroids, centroids)

    update_centroids = tf.assign(centroids, means)
    init_op = tf.initialize_all_variables()

    performance = tf.assign(centroid_distance, tf.subtract(centroids, old_centroids))
    check_stop = tf.reduce_sum(tf.abs(performance))

    with tf.Session() as sess:
        sess.run(init_op)
        for step in range(max_num_steps):
            # print "Running step " + str(step)
            sess.run(save_old_centroids)
            _, centroid_values, assignment_values = sess.run([update_centroids,
                                                              centroids,
                                                              assignments])
            sess.run(check_stop)
            current_stop_coeficient = check_stop.eval()
            # print "coeficient:", current_stop_coeficient
            if current_stop_coeficient <= stop_coeficient:
                break

        return centroid_values, assignment_values


def withinrange(temp, factor):
    """Check if all values are withing judge 30%"""
    # factor=30
    for i in range(len(temp)):
        temp[i] = float(temp[i])

    temp.sort()
    len_inp = len(temp)
    if len_inp % 2 == 0:
        median = (temp[len_inp // 2] + temp[len_inp // 2]) / 2.0
    else:
        if len_inp == 1:
            median = temp[0]
        else:
            median = temp[int(len_inp / 2)]

    for i in temp:
        if i > median * (1.0 + factor / 100) or i < median * (1.0 - factor / 100):
            return 0
    return 1


def doclassify(data, factor, start, stop):
    """select number of clusters for k-mean based on data"""
    inp = np.copy(data)
    temp = np.copy(inp)
    if withinrange(temp, factor):
        num_clusters = 1
    else:
        # number of classes/clusters
        num_clusters = 2

    # number of maxinum steps..this parameter is
    # to prevent the algorithm from running in never
    # ending loop in case it doesn't achive desiered accuracy
    max_steps = 100

    # Driver code
    vectors = []
    for i in range(1, len(inp) + 1):
        vectors.append([i, inp[i - 1]])

    vectors = np.array(np.float32(vectors))
    # print vectors
    centroids, assignments = kmeanscluster(vectors, num_clusters, max_steps)

    return assignments


def classifydata(measurement_data, scenario_count_array, factor):
    """driver function for k-mean"""
    print ("[INFO] Following Parameters are being used:")
    print ("-------------------------------------")
    # print " inputfile=%s" % inputfile
    # print " outputfile=%s" % outputfile
    # print (" factor=%d" % factor)
    # print ("-------------------------------------\n")

    print ("[INFO] Preparing Input data")
    # inputhandle=open('input/'+inputfile,'r')
    # outputhandle=open('output/'+outputfile,'w')
    # inp=[]
    # for line in inputhandle:
    #    inp.append(line)
    inp = measurement_data

    print ("[INFO] Starting Classification")
    assignments = []
    y = 0
    leninp = len(scenario_count_array)
    for i in range(leninp):
        x = y
        y = x + scenario_count_array[i]
        data = inp[x:y]
        sys.stdout.write('\r')
        sys.stdout.write("%d/%d sets completed" % (i + 1, leninp))
        sys.stdout.flush()
        assignment = doclassify(data, factor, x, y)
        ones = np.count_nonzero(assignment)
        if ones >= scenario_count_array[i] - ones:
            regular = 1
        else:
            regular = 0
        for index in range(len(assignment)):
            if assignment[index] == regular:
                assignment[index] = 1
            else:
                assignment[index] = 0

        tf.reset_default_graph()
        assignments.append(assignment)
        # showresults(data,assignments,outputhandle)
    print ("\n[INFO] Regular Irregular Classification completed\n")
    return assignments


def classify(label_data_frame, index_data_frame, inputparams):
    """Run classify for each cpu/io section in measurement summary"""
    # if ctype == "input":
    index_data_frame[index_data_frame.columns[0]] = \
        index_data_frame[index_data_frame.columns[0]].str.split('_').str[0]

    scenario_count_array = generate_scenario_array(index_data_frame.values)

    class_data_frame = pd.DataFrame()
    for column in label_data_frame:
        assignments = classifydata(label_data_frame[column].values,
                                   scenario_count_array, int(inputparams.factor))
        assignments = np.concatenate(np.array(assignments)).flatten()
        assignment_frame = pd.DataFrame(data=assignments, columns=["assignments_" + column])
        class_data_frame['assignment_' + column] = assignment_frame
        assignment_frame.to_csv(inputparams.output_folder_path + "assignments_" + column + ".csv", index=None)
    return class_data_frame
